package com.example.proyectandroid_api_n2.Data

data class Amount(
    val unit: String,
    val value: Double
)