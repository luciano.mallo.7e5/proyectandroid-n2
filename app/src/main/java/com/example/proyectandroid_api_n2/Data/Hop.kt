package com.example.proyectandroid_api_n2.Data

data class Hop(
    val add: String,
    val amount: Amount,
    val attribute: String,
    val name: String
)