package com.example.proyectandroid_api_n2.Data

data class Ingredients(
    val hops: List<Hop>,
    val malt: List<Malt>,
    val yeast: String
)