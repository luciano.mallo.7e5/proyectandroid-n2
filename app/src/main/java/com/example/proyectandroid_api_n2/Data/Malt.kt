package com.example.proyectandroid_api_n2.Data

data class Malt(
    val amount: Amount,
    val name: String
)