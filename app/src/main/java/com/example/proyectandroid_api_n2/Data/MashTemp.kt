package com.example.proyectandroid_api_n2.Data

data class MashTemp(
    val duration: Int,
    val temp: Temp
)