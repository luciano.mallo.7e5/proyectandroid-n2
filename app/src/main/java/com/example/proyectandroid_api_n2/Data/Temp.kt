package com.example.proyectandroid_api_n2.Data

data class Temp(
    val unit: String,
    val value: Int
)