package com.example.proyectandroid_api_n2.Data

data class Volume(
    val unit: String,
    val value: Int
)