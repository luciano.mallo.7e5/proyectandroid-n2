package com.example.proyectandroid_api_n2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import com.bumptech.glide.Glide
import com.example.proyectandroid_api_n2.databinding.FragmentDetailBinding


class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetailBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val result = arguments?.getParcelable<User>("User")
        if (result != null) {
            binding.Drescription.text = "Description: ${result.description}"
            binding.Name.text = "${result.name}"
            binding.Tagline.text = "${result.tag}"
            binding.Ibu.text = "IBU: ${result.ibu}"
            Glide.with(this)
                .load(result?.url)
                .placeholder(R.drawable.background)
                .into(binding.Image)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }
}


/*parentFragmentManager.setFragmentResultListener("User", this,
    FragmentResultListener { requestKey: String, result: Bundle ->


    })
return binding.root
}
*/