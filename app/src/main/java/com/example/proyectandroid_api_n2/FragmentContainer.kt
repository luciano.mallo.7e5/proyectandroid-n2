package com.example.proyectandroid_api_n2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectandroid_api_n2.databinding.FragmentContainerBinding


class FragmentContainer : androidx.fragment.app.Fragment(), OnClickListener {

    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentContainerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContainerBinding.inflate(layoutInflater)
        //inflater.inflate(R.layout.fragment_container, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAdapter = UserAdapter(getUsers(), this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    private fun getUsers(): MutableList<User> {
        val repository=Repository()
       var data = repository.GetBeers()
        val users = mutableListOf<User>()
        users.add(
            User(
                1,
                "Buzz",
                "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.",
                "https://images.punkapi.com/v2/2.png",
                "Vibrant Hoppy Saison.",
                25
            )
        )
        users.add(
            User(
                2,
                "Punk IPA 2007 - 2010",
                "Our flagship beer that kick started the craft beer revolution. " +
                        "This is James and Martin's original take on an American IPA, subverted with punchy New Zealand hops. " +
                        "Layered with new world hops to create an all-out riot of grapefruit, " +
                        "pineapple and lychee before a spiky, mouth-puckering bitter finish.",
                "https://images.punkapi.com/v2/192.png",
                "Post Modern Classic. Spiky. Tropical. Hoppy.",
                60
            )
        )
        return users
    }

    override fun onClick(user: User) {
        val action = FragmentContainerDirections.actionFragmentContainer2ToDetailFragment2(user)
        findNavController().navigate(action)

    }


}