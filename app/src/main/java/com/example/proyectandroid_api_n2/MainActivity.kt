package com.example.proyectandroid_api_n2

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController


import com.example.proyectandroid_api_n2.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_ProyectAndroidAPIn2)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}


