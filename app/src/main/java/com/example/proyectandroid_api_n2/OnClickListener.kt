package com.example.proyectandroid_api_n2

interface OnClickListener {
    fun onClick(user: User)
}