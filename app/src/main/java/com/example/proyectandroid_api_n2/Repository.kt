package com.example.proyectandroid_api_n2

import android.util.Log
import com.example.proyectandroid_api_n2.Data.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private val apiInterface = ApiInterface.create()
    fun GetBeers() {
        val call = apiInterface.getData()
        call.enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    val myData = response.body()
                    println(myData)
                }
            }
        })

    }
}