package com.example.proyectandroid_api_n2

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(val id: Long, var name: String, var description:String , var url: String, var tag:String , var ibu:Int): Parcelable